class WC:
    f = None

    def __init__(self, namafile):
        self.a = namafile

    def word_count(self):
        self.f = open(self.a,'rt')
        word = 0
        for line in self.f :
            word += len(line.split(" "))
        self.f.close()
        return word

    def line_count(self):
        self.f = open(self.a,'rt')
        lineCount = 0
        for line in self.f :
            lineCount += 1
        self.f.close()
        return lineCount

    def char_count(self):
        self.f = open(self.a,'rt')
        char = 0
        for line in self.f :
            char += len(line)
        self.f.close()
        return char
